Hello. 

First time using SDL2 so just playing around with this simple game.
As the name itself suggests, this game has nothing to do with evading bricks and I'm too lazy to rename the Repo,
so it's gonna stay like this. Free to share, download, enjoy, anything basically.
Have "fun". It has a kickass soundtrack tho. For realz.


Now more serious things:

    This is a fun little project done in SDL2 to test out the nakedness of not using UnrealEngine4 and the huge library of free assets it provides.
    
    The gameplay itself is a simple, kind of a Icy tower, clone-ish kind of thing. I do understand it is not easy to play, and it is meant to be hard.
    The reason for the (Occasional) difficulty is basically because my dear friend gave me his own music to use as the soundtrack, and it is amazing, 
    so if the player wants to listen to the whole thing he better get good :D 
    The game is just one level, with only the Start, Pause and End Screen not being the part of the gameplay.

Gameplay:
    You jump from block to block to get as high as possible, simple, but difficult. The higher you jump the better your score. 
    I didn't implement a system to keep the highscore saved between sessions, since I didn't count on anyone playing this for the score, or more
    than once ;).
    If you don't start jumping you die after 15 seconds. 
    If you hit the ground after you've started climbing - you die.

Getting started:
    Just download and play. Unless I make an installation. Then install and play. Might be a worthwile note that it is planned to be released for 
    the PC but since it seems like the Linux distros are more than capable of covering the DLLs it might be possible to play on it as well.

Graphics: 
    Everything on the screen is a .png file. So nothing fancy or heavy so I'm counting on weakest of computers to be able to play.

Licencing:
    All (3) sound effects are royalty free assets, downloaded from https://freesound.org/
    Huge thanks to the guys who created the sounds: cabled_mess, KieranKeegan and magnuswaker. Check them out on Freesound.
    Soundtrack is not licenced, but it is a fantastic work by Busstation38, a great personal friend who pours his heart and soul
    into the music he creates. It is a part of a live studio set, and I encourage everyone to check it out and show your support. 
    Check Busstation38 out here : https://www.youtube.com/channel/UCSrcMvpT2D_97mQYxVi9Xpw 

    The whole game is free to share and enjoy, make fun of or just to stare at the mindbending graphics, if it brings a smile it was worth it.




