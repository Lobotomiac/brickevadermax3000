#include "AudioSystem.h"

AudioSystem* AudioSystem::Instance = nullptr;
Uint32 AudioSystem::InstanceCounter = 0;

AudioSystem::AudioSystem()
{
	GameSoundtrack = Mix_LoadMUS("Assets/Techno Sunday Session 006 Roland Mx1, Tb-3, Push2, Tr-8sGame.wav");

	JumpSound = Mix_LoadWAV("Assets/540788__magnuswaker__boing-1.wav");
	LandSound = Mix_LoadWAV("Assets/422753__kierankeegan__landing-stone-1.wav");
	MenuPress = Mix_LoadWAV("Assets/370962__cabled-mess__click-01-minimal-ui-sounds.wav");

	bAlreadyPlayingMusic = false;

	Mix_VolumeChunk(JumpSound, 128 / 2);
	Mix_VolumeChunk(LandSound, 128 / 3);
	Mix_VolumeChunk(MenuPress, 128 / 2);
}

AudioSystem::~AudioSystem()
{
	Mix_FreeMusic(GameSoundtrack);
	Mix_FreeChunk(JumpSound);
	Mix_FreeChunk(LandSound);
	Mix_FreeChunk(MenuPress);
	GameSoundtrack = nullptr;
	JumpSound = nullptr;
	LandSound = nullptr;
	MenuPress = nullptr;
	ReleaseInstance();
}

AudioSystem * AudioSystem::GetInstance()
{
	{
		InstanceCounter++;
		if (!Instance)
		{
			Instance = new AudioSystem();
		}
		SDL_Log("%d", InstanceCounter);
		return Instance;
	}
}

void AudioSystem::ReleaseInstance()
{
	InstanceCounter--;
	if (InstanceCounter == 0 && Instance)
	{
		delete Instance;
		Instance = nullptr;
	}
}

void AudioSystem::PlayJumpSound()
{
	if (JumpSound)
	{
		Mix_PlayChannelTimed(-1, JumpSound, 0, 400);
	}
}

void AudioSystem::PlayLandSound()
{
	if (LandSound)
	{
		Mix_PlayChannelTimed(-1, LandSound, 0, 200);
	}
}

void AudioSystem::PlayMenuPressSound()
{
	if (MenuPress)
	{
		Mix_PlayChannelTimed(-1, MenuPress, 0, 100);
	}
}

void AudioSystem::PlaySoundtrack(bool bGamePaused)
{
	if (bGamePaused)
	{
		Mix_PauseMusic();
	}
	else
	{
		if (!bAlreadyPlayingMusic)
		{
			Mix_VolumeMusic(MIX_MAX_VOLUME);
			if (Mix_PlayMusic(GameSoundtrack, -1))
			{
				SDL_Log("%s", Mix_GetError());
			}
			bAlreadyPlayingMusic = true;
		}
		else
		{
			Mix_ResumeMusic();
		}
	}
}

void AudioSystem::ResetMusic()
{
	Mix_VolumeMusic(0);
	Mix_HaltMusic();
	bAlreadyPlayingMusic = false;
}