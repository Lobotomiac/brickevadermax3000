#pragma once
#include <SDL_mixer.h>
#include <SDL.h>


// We're using this amazing singleton class system to play our game soundtrack and ingame sounds
class AudioSystem
{
public:
	static AudioSystem* GetInstance();

	void ReleaseInstance();

	void PlayJumpSound();

	void PlayLandSound();

	void PlayMenuPressSound();

	void PlaySoundtrack(bool bGamePaused);

	void ResetMusic();

private:

	AudioSystem();
	~AudioSystem();

private:

	Mix_Music *GameSoundtrack = nullptr;
	Mix_Chunk *JumpSound = nullptr;
	Mix_Chunk *LandSound = nullptr;
	Mix_Chunk *MenuPress = nullptr;

	bool bAlreadyPlayingMusic;

	static AudioSystem *Instance;
	static Uint32 InstanceCounter;

};

