#include "ColliderComponent.h"


int ColliderComponent::DistanceSquared(int x1, int y1, Vector2D Collision)
{
	DeltaVector.X = Collision.X - x1;
	DeltaVector.Y = Collision.Y - y1;
	return DeltaVector.X * DeltaVector.X + DeltaVector.Y * DeltaVector.Y;
}

bool ColliderComponent::IsColliding(const Circle & PlayerLocation, const SDL_Rect & Obstacle)
{
	//Closest point on collision box
	Vector2D Collision;
	//Find closest x offset
	if (PlayerLocation.X < Obstacle.x)
	{
		Collision.X = Obstacle.x;
	}
	else if (PlayerLocation.X > Obstacle.x + Obstacle.w)
	{
		Collision.X = Obstacle.x + Obstacle.w;
	}
	else
	{
		Collision.X = PlayerLocation.X;
	}
	//Find closest y offset
	if (PlayerLocation.Y < Obstacle.y)
	{
		Collision.Y = Obstacle.y;
	}
	else if (PlayerLocation.Y > Obstacle.y + Obstacle.h)
	{
		Collision.Y = Obstacle.y + Obstacle.h;
	}
	else
	{
		Collision.Y = PlayerLocation.Y;
	}
	//If the closest point is inside the Player circle area
	if (DistanceSquared(PlayerLocation.X, PlayerLocation.Y, Collision) < PlayerLocation.Radius * PlayerLocation.Radius)
	{
		//This Obstacle and the Player have collided
		return true;
	}
	return false;
}
