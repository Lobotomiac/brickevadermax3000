#pragma once

#include "SDL.h"
#include "Vector2D.h"

struct Circle
{
	int X;
	int Y;
	int Radius;
};

// If not clear from the name, this is the component that checks whether two objects (Player and obstacle) are colliding
class ColliderComponent
{
public:
	bool IsColliding(const Circle& PlayerLocation, const SDL_Rect& Obstacle);

	Vector2D GetCollisionDeltaVector() const { return DeltaVector; }

private:
	Vector2D DeltaVector;

	int DistanceSquared(int x1, int y1, Vector2D Collision);

};