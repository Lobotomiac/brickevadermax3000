#pragma once

#include "SDL.h"

// Base class for objects rendered on screen 
class Entity
{
public:
	virtual void Render(SDL_Renderer* Renderer) = 0;

public:
	SDL_Texture *Texture = nullptr;

	SDL_Rect SourceRect, DestinationRect;
	int DefaultVelocity = 1;
	bool bIsActive = true;

};