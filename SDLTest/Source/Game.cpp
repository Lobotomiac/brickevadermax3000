#include "Game.h"

const int Game::SCREEN_HEIGHT = 720; //TODO Perhaps transfer to a separate .h (graphics or smh)
const int Game::SCREEN_WIDTH = 1280;


Game::Game()
{
	Window = nullptr;
	Surface = nullptr;
	Renderer = nullptr;
	Player = nullptr;
	Obstacles = nullptr;
	Score = nullptr;
	Time = nullptr;
	PauseScreen = nullptr;
	StartScreen = nullptr;
	GameOverScreen = nullptr;
	MusicInstance = nullptr;

	bGameStarted = false;
}

Game::~Game()
{
	CleanGameObjects();
	CleanGameScreenAssets();
	SDL_DestroyRenderer(Renderer);
	SDL_DestroyWindow(Window);
	Window = nullptr;
	Renderer = nullptr;
	TTF_Quit();
	Mix_CloseAudio();
	Mix_Quit();
	SDL_Quit();
	std::cout << "Game destructor Ran!" << std::endl;
}


bool Game::InitializeSDL()
{
	// Initialise SDL (since we can't use any of its goodies without initializing first)
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		std::cout << "SDL_Init failed! SDL_Error: " << SDL_GetError() << std::endl;
		return false;
	}
	
	// Create window
	Window = SDL_CreateWindow(
		"Amazing best game ever",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		SDL_WINDOW_SHOWN
	);
	
	if (!Window)
	{
		std::cout << "SDL_CreateWindow failed! SDL_Error: " << SDL_GetError() << std::endl;
		return false;
	}
	// Get the window surface (to draw on)
	/*A new surface will be created with the optimal format for the window, if necessary.
		*This surface will be freed when the window is destroyed.Do not free this surface.
		*This surface will be invalidated if the window is resized.
		*After resizing a window this function must be called again to return a valid surface.
		*You may not combine this with 3D or the rendering API on this window.*/
	if (!(Surface = SDL_GetWindowSurface(Window)))
	{
		std::cout << "SDL_GetWindowSurface failed! SDL_Error: " << SDL_GetError() << std::endl;
		return false;
	}

	// Creating a 2d rendering context for the window
	if (!(Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED)))
	{
		std::cout << "SDL_CreateRenderer failed! SDL_Error: " << SDL_GetError() << std::endl;
		return false;
	}
	//TODO Create a proper background // Play around with palette to find a pleasing one 
	SDL_SetRenderDrawColor(Renderer, 89, 210, 254, 255);
	
	if (TTF_Init())
	{
		std::cout << "TTF_Init() failed! Error: " << TTF_GetError() << std::endl;
		return false;
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048))
	{
		std::cout << "SDL_mixer failed! Error: " << Mix_GetError() << std::endl;
		return false;
	}

	std::cout << "System Initialized! Hooray" << std::endl;
	return true;
}


bool Game::InitializeGame()
{
	Player = new PlayerObject("Assets/PlayerBall.png", Renderer, SCREEN_WIDTH, SCREEN_HEIGHT);
	if (!Player)
	{
		std::cout << "Failed to instantiate Player! SDL_Error: " << SDL_GetError() << std::endl;
		return false;
	}

	Obstacles = new ObstacleManager(Renderer, SCREEN_HEIGHT, SCREEN_WIDTH);
	if (!Obstacles)
	{
		std::cout << "Failed to instantiate Obstacles! SDL_Error: " << SDL_GetError() << std::endl;

		return false;
	}

	ObstacleArray = Obstacles->GetObstacleArray();

	Score = new ScoreDisplay();
	if (!Score)
	{
		std::cout << "Failed to instantiate Score! SDL_Error: " << SDL_GetError() << std::endl;
		return false;
	}
	
	Time = new Timer();
	if (!Time)
	{
		std::cout << "Failed to instantiate Timer! SDL_Error: " << SDL_GetError() << std::endl;
		return false;
	}

	MusicInstance = AudioSystem::GetInstance();
	return true;
}


bool Game::InitializeScreens()
{
	StartScreen = new MenuScreen(StartImagePath, Renderer);
	PauseScreen = new MenuScreen(PauseImagePath, Renderer);
	GameOverScreen = new MenuScreen(EndImagePath, Renderer);
	if (!(StartScreen && PauseScreen && GameOverScreen))
	{
		std::cout << "Failed to instantiate Start/Pause/End screens! SDL_Error: " << SDL_GetError() << std::endl;
		return false;
	}
	return true;
}


void Game::ResetGame()
{
	CleanGameObjects();
	InitializeGame();
	bGameStarted = true;
	Player->SetGamePaused(false);
	MusicInstance->ResetMusic();
	
}


// The main GameLoop, covering start, play and quit sections
void Game::Play()
{
	if (!InitializeSDL())
	{
		std::cout << "InitializeSDL ERROR" << std::endl;
	}
	if (!InitializeScreens())
	{
		std::cout << "InitializeScreens ERROR" << std::endl;
	}
	if (!InitializeGame())
	{
		std::cout << "InitializeGame ERROR" << std::endl;
	}

	
	while (GetContinuePlaying())
	{
		while (GetPlayerAlive() && GetContinuePlaying())
		{
			Update();
			RenderDisplay();

			Time->UpdateTime(GetGamePaused());
			// Next little block is basically a lazy way to ensure the player starts jumping onto obstacles
			// Completely understandable he wants to hear the whole song, but work for it ;)
			int CurrentTime = Time->GetUnpausedTime();
			if (CurrentTime > 15000 && !Player->GetStartedClimbing())
			{
				Player->SetGameOver();
				Player->SetGamePaused(true);
			}
		}

		Update();
		RenderDisplay();

		Time->UpdateTime(GetGamePaused());

		if (Player->GetRestartGame() && !GetPlayerAlive())
		{
			ResetGame();
		}
	}
}

// This function will be to update every objects movement
void Game::Update()
{
	Player->Update();
	if (!GetGamePaused())
	{
		bGameStarted = true;

		// We'll be using this counter to check if the player collided with an obstacle to allow him to rest
		// (easier than having many checks on the player himself)
		int HasCollision = 0; 
		Obstacles->Update(Player->GetSurplusVelocity());	
		for (Obstacle* AnObstacle : ObstacleArray)
		{
			if (AnObstacle->GetIsActive())
			{
				HasCollision += Player->CheckCollision(AnObstacle->GetObstacleRect());
			}
		}
		Player->SetbPlayerResting(HasCollision);
		Score->Update();
	}
	MusicInstance->PlaySoundtrack(GetGamePaused());
}

// Rendering images to the screen
void Game::RenderDisplay()
{
	if (!GetGamePaused())
	{
		SDL_RenderClear(Renderer);
		Obstacles->RenderObstacles(Renderer);
		Player->Render(Renderer);
		Score->Render(Renderer);
	}
	else
	{
		if (!bGameStarted)
		{
			StartScreen->Render(Renderer);
		}
		else if (!GetPlayerAlive())
		{
			GameOverScreen->Render(Renderer);
			Score->Render(Renderer);
		}
		else
		{
			PauseScreen->Render(Renderer);
		}
	}	
	SDL_RenderPresent(Renderer);
}


void Game::CleanGameObjects()	// Hopefully this is self explanatory
{
	delete Player;
	delete Obstacles;
	delete Score;
	delete Time;
	MusicInstance->ReleaseInstance();
	for (auto Obst : ObstacleArray)
	{
		Obst = nullptr;
	}
	Player = nullptr;
	Obstacles = nullptr;
	Score = nullptr;
	Time = nullptr;
	MusicInstance = nullptr;

	std::cout << "CleanGameObjects() ran!" << std::endl;
}

// Cleaning the different Screens we're using in our game
void Game::CleanGameScreenAssets()
{
	delete StartScreen;
	delete PauseScreen;
	delete GameOverScreen;
	StartScreen = nullptr;
	PauseScreen = nullptr;
	GameOverScreen = nullptr;

	std::cout << "CleanGameScreenAssets() ran!" << std::endl;
}

