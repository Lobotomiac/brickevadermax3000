#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <array>
#include <iostream>
#include <stdio.h>
#include "PlayerObject.h"
#include "ObstacleManager.h"
#include "ScoreDisplay.h"
#include "MenuScreen.h"
#include "Timer.h"
#include "AudioSystem.h"


class Game
{
public:
	Game();
	~Game();
	void Play();

public:
	// Setting the Screen dimensions to a constant as to not screw it up later
	static const int SCREEN_WIDTH;
	static const int SCREEN_HEIGHT;

private:
	bool InitializeSDL();

	bool InitializeGame();

	bool InitializeScreens();

	void Update();

	void RenderDisplay();

	bool GetGamePaused() const { return Player->GetGamePaused(); };

	bool GetGameStarted() const { return bGameStarted; };

	bool GetPlayerAlive() const { return Player->GetPlayerAlive(); };

	bool GetContinuePlaying() const { return Player->GetContinuePlaying(); }

	void ResetGame();

	void CleanGameScreenAssets();

	void CleanGameObjects();

private:
	SDL_Window *Window;
	SDL_Surface *Surface;
	SDL_Renderer *Renderer;
	PlayerObject *Player;
	ObstacleManager *Obstacles;
	ScoreDisplay *Score;
	Timer *Time;
	MenuScreen *PauseScreen;
	MenuScreen *StartScreen;
	MenuScreen *GameOverScreen;
	AudioSystem *MusicInstance;
	
	std::array<Obstacle*, 9> ObstacleArray;
	const char* PauseImagePath = "Assets/PauseScreen.png";
	const char* StartImagePath = "Assets/StartScreen.png";
	const char* EndImagePath = "Assets/EndScreen.png";
	bool bGameStarted;

};

