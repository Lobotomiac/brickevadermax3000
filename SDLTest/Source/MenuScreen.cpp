#include "MenuScreen.h"

MenuScreen::MenuScreen(const char* TexturePath, SDL_Renderer * Renderer)
{
	Texture = TextureManager::LoadTexture(TexturePath, Renderer);
}

MenuScreen::~MenuScreen()
{
	SDL_DestroyTexture(Texture);
	Texture = nullptr;
}

void MenuScreen::Render(SDL_Renderer * Renderer)
{
	SDL_RenderCopy(Renderer, Texture, nullptr, nullptr);
}
