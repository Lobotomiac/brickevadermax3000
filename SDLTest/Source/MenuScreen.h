#pragma once

#include <SDL.h>
#include "TextureManager.h"

class MenuScreen
{
public:
	MenuScreen(const char* TexturePath, SDL_Renderer* Renderer);
	~MenuScreen();

	void Render(SDL_Renderer* Renderer);
	
private:
	SDL_Texture *Texture = nullptr;

};