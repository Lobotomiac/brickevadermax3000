#include "Obstacle.h"


Obstacle::Obstacle(SDL_Renderer* Renderer, int WidthFactor, int HeightFactor, int ScreenHeight, int StartLocationY)
{
	FallVelocity = DefaultVelocity; 
	GameScreenHeight = ScreenHeight;
	
	Texture = TextureManager::LoadTexture(TexturePath, Renderer);

	// Something to measure our individual tiles in the texture 
	int TileWidth = 64; // We're having this here just because we know exactly how much 1 tile in the Texture is wide / high 

	SDL_QueryTexture(Texture, nullptr, nullptr, &SourceRect.w, &SourceRect.h);
	// Setting our Source XY to point to the start of the required nth tile
	SourceRect.x = SourceRect.w - WidthFactor * TileWidth;
	SourceRect.y = SourceRect.h - HeightFactor * TileWidth;
	
	// Setting our w,h to the required size for properly displaying tile sizes
	DestinationRect.w = TileWidth * WidthFactor;
	DestinationRect.h = TileWidth * HeightFactor;
	DestinationRect.x = 0;
	DestinationRect.y = StartLocationY;
	bIsActive = false;
}

Obstacle::~Obstacle()
{
	SDL_DestroyTexture(Texture);
	Texture = nullptr;
}


void Obstacle::UpdateTransform(int Acceleration)
{
	DestinationRect.y += FallVelocity - Acceleration;
}


// We're giving the obstacles extra velocity if there is any to simulate player jumping higher, rather than just bouncing off the top of the screen
void Obstacle::Update(int Acceleration)
{
	if (bIsActive)
	{
		if (DestinationRect.y <= GameScreenHeight)
		{
			UpdateTransform(Acceleration);
		}
		else
		{
			bIsActive = false;
		}
	}
}

void Obstacle::Render(SDL_Renderer* Renderer)
{
	if (!Texture)
	{
		SDL_Log("no texture");
	}
	if (SDL_RenderCopy(Renderer, Texture, &SourceRect, &DestinationRect))
	{
		SDL_Log(SDL_GetError());
	}
}