#pragma once

#include "Entity.h"
#include "TextureManager.h"
#include <iostream>



class Obstacle : public Entity
{
public:
	Obstacle(SDL_Renderer* Renderer, int WidthFactor, int HeightFactor, int GameScreenHeight, int StartLocationY);
	~Obstacle();

	void Update(int Acceleration);

	void Render(SDL_Renderer* Renderer) override;

	void SetStartingPosition(int X, int Y) { DestinationRect.x = X; DestinationRect.y = Y; bIsActive = true; };

	bool GetIsActive() const { return bIsActive; };

	SDL_Rect GetObstacleRect() const { return DestinationRect; };

protected:
	int FallVelocity;
	int GameScreenHeight;
	const char* TexturePath = "Assets/Debris.png";
	
protected:
	void UpdateTransform(int Acceleration);

};