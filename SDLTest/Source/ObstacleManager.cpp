#include "ObstacleManager.h"


ObstacleManager::ObstacleManager(SDL_Renderer* Renderer, int ScreenHeight, int ScreenWidth)
{
	LastSpawnedObstacle = nullptr;
	SpawnLocationY = -192;
	SpawnDistanceHorizontalMax = 700;
	SpawnDistanceVerticalMax = 128;
	GameScreenHeight = ScreenHeight;
	GameScreenWidth = ScreenWidth;
	PopulateObstacleArray(Renderer);
}

ObstacleManager::~ObstacleManager()
{
	DeleteObstacleArray();
	LastSpawnedObstacle = nullptr;
}

void ObstacleManager::PopulateObstacleArray(SDL_Renderer* Renderer)
{
	int ObstaclesSize = int (ObstacleArray.size());
	int ContainerIndex = 0;

	// Setting the array start at 1 so it suits our shape forming mechanism for the obstacles
	for (int WidthFactor = 1; WidthFactor < 4; WidthFactor++)
	{
		for (int HeightFactor = 1; (ContainerIndex < ObstaclesSize && HeightFactor < 4); HeightFactor++, ContainerIndex++)
		{
			ObstacleArray[ContainerIndex] = new Obstacle(Renderer, WidthFactor, HeightFactor, GameScreenHeight, SpawnLocationY);
		}
	}
}

// Spawn checking mechanism is here because two reasons - 1: small enough 2: I couldn't think of a good fitting name for the function
void ObstacleManager::Update(int Acceleration = 0)
{
	if (LastSpawnedObstacle)
	{
		if (SpawnLocationY + SpawnDistanceVerticalMax < LastSpawnedObstacle->GetObstacleRect().y)
		{
			SpawnValidObstacle();
		}
	}
	else
	{
		SpawnValidObstacle();
	}

	UpdateObstacles(Acceleration);
}


//************************************
// Method:    SpawnValidObstacle
// FullName:  ObstacleManager::SpawnValidObstacle
// Access:    private 
// Returns:   void
// Qualifier:
// Parameter: const int & Time
// 
// This function here finds a valid obstacle (one that isn't on screen and is generally good to spawn),
// keeps track of how many obstacles are spawned, and the Obstacle we've spawned before, allowing for tracking it's position and influence further spawning etc.
// Controlled randomization of spawn points is essential to making this playable and slightly enjoyable
//************************************
void ObstacleManager::SpawnValidObstacle()
{
	int ObstaclesSize = int (ObstacleArray.size());
	bool bFoundValidObstacle = false;
	Obstacle *ValidObstacle = nullptr;
	std::srand(SDL_GetTicks());	 
	int ActiveObstacles = 0;	
	for (auto Obstacle : ObstacleArray)
	{
		if (Obstacle->GetIsActive())
		{
			ActiveObstacles++;
		}
	}
	if (ActiveObstacles >= ObstaclesSize - 1)
	{
		SDL_Log("All Obstacles Active! Unable to spawn any");
		return;	// Making sure we don't try to find a valid one since all of them are already spawned
	}
	// Finding a good obstacle to spawn (not currently on screen)
	do
	{
		int ONum = rand() % (ObstaclesSize - 1);
		ValidObstacle = ObstacleArray[ONum];
		if (ValidObstacle && !ValidObstacle->GetIsActive())
		{
			bFoundValidObstacle = true;
		}
	} while (!bFoundValidObstacle);
	// Here we randomize our spawn point, making sure it sits comfortably inside the players screen
	// Close enough to last spawned obstacle to ensure Player ability to jump to it 
	bool bIsValidSpawnPoint = false;

	if (ValidObstacle)
	{
		SDL_Rect SpawnRect = ValidObstacle->GetObstacleRect();
		int SpawnDest, SpawnDistance;
		do 
		{
			SpawnDest = rand() % (GameScreenWidth - SpawnRect.w);

			// Since there can only be distances between spawns if a spawn previously happened at all this is all we need to check for 
			if (LastSpawnedObstacle)
			{
				SpawnDistance = SpawnDest - LastSpawnedObstacle->GetObstacleRect().x;

				// Making sure the distance between the spawns is juuuust right
				if (SpawnDistance > SpawnDistanceHorizontalMax || SpawnDistance < -SpawnDistanceHorizontalMax)
				{
					continue;
				}

				if (SpawnDistance >= 0)
				{
					if (SpawnDistance < LastSpawnedObstacle->GetObstacleRect().w)
					{
						continue;
					}
				}
				else
				{
					if (SpawnDistance > -SpawnRect.w)
					{
						continue;
					}
				}
			}

			bIsValidSpawnPoint = true;

		} while (!bIsValidSpawnPoint);
		
		SpawnRect.x = SpawnDest;
		ValidObstacle->SetStartingPosition(SpawnRect.x, SpawnLocationY);

		LastSpawnedObstacle = ValidObstacle;
	}
	else
	{
		std::cout << "No Valid Obstacle To spawn" << std::endl;
	}
}


void ObstacleManager::UpdateObstacles(int Acceleration)
{
	for (Obstacle* Debris : ObstacleArray)
	{
		if (Debris && Debris->GetIsActive())
		{
			Debris->Update(Acceleration);
		}
	}
}


void ObstacleManager::RenderObstacles(SDL_Renderer* Renderer)	
{
	for (Obstacle* Debris : ObstacleArray)
	{
		if (Debris && Debris->GetIsActive())
		{
			Debris->Render(Renderer);
		}
	}
}


void ObstacleManager::DeleteObstacleArray()
{
	for (Obstacle* Debris : ObstacleArray)
	{
		delete Debris;
		Debris = nullptr;
	}
}
