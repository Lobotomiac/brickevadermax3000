#pragma once

#include "Obstacle.h"
#include <array>

class ObstacleManager
{
public:
	ObstacleManager(SDL_Renderer* Renderer, int ScreenHeight, int ScreenWidth);
	~ObstacleManager();

	void Update(int Acceleration);

	void RenderObstacles(SDL_Renderer* Renderer);

	void DeleteObstacleArray();

	std::array<Obstacle*, 9> GetObstacleArray() { return ObstacleArray; };

private:
	void PopulateObstacleArray(SDL_Renderer* Renderer);

	void SpawnValidObstacle();

	void UpdateObstacles(int Acceleration);

private:
	Obstacle* LastSpawnedObstacle;

	std::array<Obstacle*, 9> ObstacleArray;
	int SpawnDistanceHorizontalMax;		// Hardcoded this as a jump distance is measured to be an optimal distance between objects
	int SpawnDistanceVerticalMax;		// We'll just hardcode this as a size of 2squares of obstacle textures (seems like an optimal distance between objects)
	int SpawnLocationY;		// This we'll hardcode as a distance of 3 consecutive lengths of obstacle square size 
	int GameScreenHeight;
	int GameScreenWidth;

};

