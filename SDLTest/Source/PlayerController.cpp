#include "PlayerController.h"


PlayerController::PlayerController(int SCREENWIDTH, int SCREENHEIGHT, int Rest)
{
	bRestartGame = false;
	bStartedClimbing = false;
	bIsResting = true;
	bIsOnGround = true;
	bGamePaused = true;
	bPlayerAlive = true;
	bIdle = true;
	BounceFactor = 2;
	JumpStrenght = 20;
	SurplusYVelocity = 0;
	RestVelocity = Rest;
	ScreenHeight = SCREENHEIGHT;
	ScreenWidth = SCREENWIDTH;
	ScoreInstance = ScoreSystem::GetInstance();
	AudioInstance = AudioSystem::GetInstance();
}

PlayerController::~PlayerController()
{
	AudioInstance->ReleaseInstance();
	AudioInstance = nullptr;
	ScoreInstance->ReleaseInstance();
	ScoreInstance = nullptr;
}


int PlayerController::UpdateTransform(SDL_Rect &PlayerRect)
{
	VerticalClimbTracker = PlayerRect.y;

	Move(PlayerRect);
	return Velocity.X;
}

// This function makes sure our player collision with the obstacles results in the proper correction in their movement
// DeltaVector is the value of how much the Player's center is above the colliding object, letting us know how to adjust the players position further
void PlayerController::AdjustTransform(const Vector2D& DeltaVector, SDL_Rect& PlayerRect, const int& Radius)
{ 
	if (DeltaVector.Y)
	{
		/// this next block makes our player "rest" on top of a falling obstacle
		PlayerRect.y -= Radius - DeltaVector.Y - 1; // - 1 makes the player constantly "stick" to the obstacle it's on/allowing knowing when we're off it
		
		if (ScoreInstance && !bPlayerLanded)
		{
			ScoreInstance->UpdateScore(PlayerRect.y);
			AudioInstance->PlayLandSound();
			bPlayerLanded = true;
		}
		Velocity.Y = RestVelocity;
		bIsResting = true;
		bStartedClimbing = true;
		bIsOnGround = false;
	}
}

void PlayerController::Move(SDL_Rect &PlayerRect)
{
	if (!bIsResting)
	{
		Velocity.Y++; // This is a simple gravity simulating line
	}
	else
	{
		Velocity.Y = RestVelocity;	// Aligning our  movement with the movement of the falling obstacles
	}

	PlayerRect.x += Velocity.X;
	PlayerRect.y += Velocity.Y;
	
	SurplusYVelocity = 0;

	// This block check if the player movement would end up outside of the screen and makes sure we stay inside the screen during game play
	if (PlayerRect.x >= ScreenWidth - PlayerRect.w)
	{
		PlayerRect.x = ScreenWidth - PlayerRect.w;	
		BounceHorizontal();
	}
	else if (PlayerRect.x <= 0)
	{
		PlayerRect.x = 0;	
		BounceHorizontal();
	}
	if (PlayerRect.y >= ScreenHeight - PlayerRect.h)
	{ 
		PlayerRect.y = ScreenHeight - PlayerRect.h;
		bIsOnGround = true;
		bIsResting = true;
		Velocity.Y = 0;
	}
	else if (PlayerRect.y <= 0)
	{
		PlayerRect.y = 0;
		SurplusYVelocity = Velocity.Y;
	}
}


void PlayerController::MoveRight()
{
	if (++Velocity.X > MaxVelocity)
	{
		Velocity.X = MaxVelocity;
	}
	bIdle = false;
}


void PlayerController::MoveLeft()
{
	if (--Velocity.X < -MaxVelocity)
	{
		Velocity.X = -MaxVelocity;
	}
	bIdle = false;
}

void PlayerController::Jump()
{
	if (bIsResting || bIsOnGround)
	{
		AudioInstance->PlayJumpSound();
		if (Velocity.Y > -MaxVelocity)
		{
			Velocity.Y = -JumpStrenght;
		}
		if (ScoreInstance)
		{
			ScoreInstance->SetScoreBaseValue(VerticalClimbTracker);
		}
		bPlayerLanded = false;
		bIsResting = false;
		bIdle = false;
		bIsOnGround = false;
	}
}


bool PlayerController::ReceiveInput()
{
	SDL_Event ControllInputEvent;

	while (SDL_PollEvent(&ControllInputEvent)) // Has the player pressed the giant X in the screen corner to exit the game?
	{
		if (ControllInputEvent.type == SDL_QUIT)
		{
			printf("QuitEvent called\n");
			return false;
		}
	}
	
	bIdle = true;
	const Uint8* Keystates = SDL_GetKeyboardState(nullptr);
	if (!GetIsGamePaused())
	{
		if (Keystates[SDL_SCANCODE_A])
		{
			MoveLeft();
		}
		if (Keystates[SDL_SCANCODE_D])
		{
			MoveRight();
		}
		if (Keystates[SDL_SCANCODE_SPACE])
		{
			Jump();
		}
	}
	if ((Keystates[SDL_SCANCODE_ESCAPE] || Keystates[SDL_SCANCODE_P]) && bPlayerAlive)// pause/unpause game 
	{
		bGamePaused = !bGamePaused;
		AudioInstance->PlayMenuPressSound();
		SDL_Log("GamePaused? %d", bGamePaused);
		SDL_Delay(MenuButtonDelay);	
		return true;
	}
	if (Keystates[SDL_SCANCODE_Q])	// Quit Game
	{
		if (bGamePaused)
		{
			AudioInstance->PlayMenuPressSound();
			return false;
		}
	}
	if (Keystates[SDL_SCANCODE_Y])	// Restart Game
	{
		if (bGamePaused)
		{
			bRestartGame = true;
			AudioInstance->PlayMenuPressSound();
			SDL_Delay(MenuButtonDelay);
		}
	}

	// Slow down the movement to a stop, no need to handle Y velocity here since it is getting handled as soon as our character is off the ground
	if (bIdle && !GetIsGamePaused())
	{
		if (Velocity.X > 0)
		{
			Velocity.X--;
		}
		else if (Velocity.X < 0)
		{
			Velocity.X++;
		}
	}

	return true;
}