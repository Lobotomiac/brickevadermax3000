#pragma once
#include "SDL.h"
#include <iostream>
#include "Vector2D.h"
#include "ScoreSystem.h"
#include "AudioSystem.h"

/*
 *We'll be using this class to Receive and Handle player input via SDL_Events,
 * and to move our Pawn accordingly. Also to emit sound on certain actions 
 */


class PlayerController
{
public:
	PlayerController(int SCREENWIDTH, int SCREENHEIGHT, int Rest);
	~PlayerController();
	// Here we grab the input the player provides, while letting the calling object to know if the input is valid and\or the player decided to quit the game
	// ALSO counter intuitively we use the AudioSystem to emit sounds if we use the appropriate keys 
	bool ReceiveInput();

	// And Update his position accordingly 
	int UpdateTransform(SDL_Rect &PlayerRect);

	// Fix transform after collision
	void AdjustTransform(const Vector2D& DeltaVector, SDL_Rect& PlayerRect, const int& Radius);

	// Provide insight about has the player hit the ground after climbing began
	bool PlayerFell() { return !(bStartedClimbing && bIsOnGround); };

	Vector2D GetVelocity() const { return Velocity; };

	bool GetIsGamePaused() const { return bGamePaused; };

	bool GetRestartGame() const { return bRestartGame; };

	int GetSurplusVelocity() const { return SurplusYVelocity; };

	bool GetbStartedClimbing() const { return bStartedClimbing; };

	void SetGamePaused(bool Paused) { bGamePaused = Paused; };

	void SetbIsResting(bool IsPlayerResting) { bIsResting = IsPlayerResting; };

	void SetbPlayerAlive(bool PlayerAlive) { bPlayerAlive = PlayerAlive; };
private:
	// Hopefully this is self explanatory
	void MoveRight();
	void MoveLeft();
	void Jump();
	void BounceHorizontal() { Velocity.X = -(Velocity.X / BounceFactor); }
	void Move(SDL_Rect &PlayerRect);

protected:
	ScoreSystem* ScoreInstance = nullptr;
	AudioSystem* AudioInstance = nullptr;

	Vector2D Velocity;
	int VerticalClimbTracker;
	int BounceFactor;
	int JumpStrenght;
	int MaxVelocity = 20;
	int SurplusYVelocity;
	int RestVelocity;	// Velocity of free falling obstacles
	int ScreenWidth, ScreenHeight;
	const int MenuButtonDelay = 110; // How long will we delay the program running

	bool bIdle;
	bool bStartedClimbing;
	bool bIsOnGround;	// Has player hit the ground?
	bool bIsResting;	// Unlike bIsGrounded this marks every instance of player resting on something
	bool bPlayerLanded; // Keeping track if the player has just landed onto a surface or has he been been sittin' there a mighty long time
	bool bGamePaused;
	bool bRestartGame;
	bool bPlayerAlive;

};