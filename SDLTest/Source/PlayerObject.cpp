#include "PlayerObject.h"

/* 
 * This is a class that we'll be using for rendering and interacting in general with the players pawn (character) 
 */


PlayerObject::PlayerObject(const char* ObjectTexture, SDL_Renderer* Renderer, int ScreenWidth, int ScreenHeight)
{
	Texture = TextureManager::LoadTexture(ObjectTexture, Renderer);

	SDL_QueryTexture(Texture, NULL, NULL, &SourceRect.w, &SourceRect.h); 
	SourceRect.x = 0;
	SourceRect.y = 0;

	Rotation = 0.0;

	DestinationRect.w = SourceRect.w;
	DestinationRect.h = SourceRect.h;
	DestinationRect.x = (ScreenWidth - DestinationRect.w) / 2;
	DestinationRect.y = ScreenHeight - DestinationRect.h;
	
	PlayerArea.Radius = SourceRect.w / 2;

	MoveComponent = new PlayerController(ScreenWidth, ScreenHeight, DefaultVelocity);
	CollisionComp = new ColliderComponent();

	bPlayerAlive = true;
	bContinuePlaying = true;
}


PlayerObject::~PlayerObject()
{
	SDL_DestroyTexture(Texture);
	delete MoveComponent;
	delete CollisionComp;
	MoveComponent = nullptr;
	CollisionComp = nullptr;
	Texture = nullptr;
}


void PlayerObject::Render(SDL_Renderer* Renderer)
{
	SDL_RenderCopyEx(Renderer, Texture, &SourceRect, &DestinationRect, Rotation, nullptr, SDL_FLIP_NONE);
}


bool PlayerObject::CheckCollision(const SDL_Rect & ObstacleRect)
{
	Vector2D PlayerVelocity = MoveComponent->GetVelocity();

	int ContactTolerance = 10;

	if (PlayerVelocity.Y >= 0)	// Is the player falling, or "juuuust sliding"?
	{
		if (CollisionComp->IsColliding(PlayerArea, ObstacleRect))
		{
			if (PlayerArea.X + ContactTolerance > ObstacleRect.x && PlayerArea.X - ContactTolerance < ObstacleRect.x + ObstacleRect.w) // Is the middle of player inside of obstacle
			{
				if ((DestinationRect.y + DestinationRect.h) - ObstacleRect.y <= PlayerVelocity.Y) // Has the player landed on top or just missed 
				{
					MoveComponent->AdjustTransform(CollisionComp->GetCollisionDeltaVector(), DestinationRect, PlayerArea.Radius);
					PlayerArea.X = DestinationRect.x + DestinationRect.w / 2;
					PlayerArea.Y = DestinationRect.y + DestinationRect.h / 2;
					
					return true;
				}
			}
		}
	}
	return false;
}


void PlayerObject::UpdateTransform()
{
	Rotation += MoveComponent->UpdateTransform(DestinationRect);
	PlayerArea.X = DestinationRect.x + DestinationRect.w / 2;
	PlayerArea.Y = DestinationRect.y + DestinationRect.h / 2;
}

// Update our position, state etc
bool PlayerObject::Update()
{
	bContinuePlaying = MoveComponent->ReceiveInput();
	if (!GetGamePaused())
	{
		UpdateTransform();
		bPlayerAlive = MoveComponent->PlayerFell();
		if (!bPlayerAlive)
		{
			MoveComponent->SetGamePaused(true);
			SDL_Log("Player Fell ||| GAME OVER");
		}
	}
	return bPlayerAlive;
}
