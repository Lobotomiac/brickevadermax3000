#pragma once

#include "ColliderComponent.h"
#include "TextureManager.h"
#include "PlayerController.h"
#include "Entity.h"
#include <iostream>


class PlayerObject : public Entity
{
public: 
	PlayerObject(const char* ObjectTexture, SDL_Renderer* Renderer, int ScreenWidth, int ScreenHeight);
	~PlayerObject();

	bool Update();
	void Render(SDL_Renderer* Renderer) override; 

	Circle GetPlayerArea() const { return PlayerArea; };

	bool GetIsActive() const { return bIsActive; };

	SDL_Rect GetPlayerRect() const { return DestinationRect; };

	bool GetGamePaused() const { return MoveComponent->GetIsGamePaused(); };

	bool GetContinuePlaying() const { return bContinuePlaying; };

	bool GetPlayerAlive() const { return bPlayerAlive; };

	bool GetStartedClimbing() { return MoveComponent->GetbStartedClimbing(); };

	bool GetRestartGame() const { return MoveComponent->GetRestartGame(); };

	int GetSurplusVelocity() const { return MoveComponent->GetSurplusVelocity(); };

	void SetPlayerRect(const SDL_Rect& SafeRect) { DestinationRect = SafeRect; };

	void SetbPlayerResting(bool Resting) { MoveComponent->SetbIsResting(Resting); };

	void SetGamePaused(bool GamePaused) { MoveComponent->SetGamePaused(GamePaused); };

	void SetGameOver() { bPlayerAlive = false; MoveComponent->SetbPlayerAlive(false); };

	bool CheckCollision(const SDL_Rect &ObstacleRect);

private:
	void UpdateTransform();

private:
	Circle PlayerArea;
	double Rotation;
	bool bPlayerAlive;
	bool bContinuePlaying;

	PlayerController* MoveComponent = nullptr;
	ColliderComponent* CollisionComp = nullptr;

};

