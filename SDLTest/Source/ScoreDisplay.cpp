#include "ScoreDisplay.h"


ScoreDisplay::ScoreDisplay()
{
	FontColor = { 0, 0, 0 };
	FontSize = 50;
	ScoreValue = 2; // setting this as 2 just so it calls TTF_RenderText_Solid once at start
	ScoreText = "0";
	ScoreInstance = ScoreSystem::GetInstance();
	ScoreInstance->ResetScore();
	Font = TTF_OpenFont(FontFilePath, FontSize);
	if (!Font)
	{
		SDL_Log("Font ERROR : %s", SDL_GetError());
	}
	bShouldUpdateTexture = true;
}

ScoreDisplay::~ScoreDisplay()
{
	TTF_CloseFont(Font);
	Font = nullptr;
	SDL_DestroyTexture(Texture);
	Texture = nullptr;
	SDL_FreeSurface(ScoreSurface);
	ScoreSurface = nullptr;
	ScoreInstance->ReleaseInstance();
	ScoreInstance = nullptr;
}


void ScoreDisplay::Render(SDL_Renderer* Renderer)
{ // Make a mechanism to prevent creating texture each frame
	if (bShouldUpdateTexture)
	{
		Texture = SDL_CreateTextureFromSurface(Renderer, ScoreSurface);
		if (!Texture)
		{
			SDL_Log("ScoreDisplay Texture ERROR : %s", SDL_GetError());
		}
		else
		{
			SDL_QueryTexture(Texture, nullptr, nullptr, &DestinationRect.w, &DestinationRect.h);
		}
		bShouldUpdateTexture = false;
	}
	
	if (SDL_RenderCopy(Renderer, Texture, nullptr, &DestinationRect))
	{
		SDL_Log("ScoreDisplay SDL_RenderCopy ERROR : %s", SDL_GetError());
	}
}

void ScoreDisplay::Update()
{
	int NewScore = ScoreInstance->GetScore();
	if (ScoreValue != NewScore)
	{
		ScoreValue = NewScore;
		ScoreText = "SCORE : " + std::to_string(ScoreValue);
		
		if (!(ScoreSurface = TTF_RenderText_Solid(Font, ScoreText.c_str(), FontColor)))
		{
			SDL_Log("ScoreDisplay Update ERROR : %s", SDL_GetError());
		}
		bShouldUpdateTexture = true;
	}	
}


