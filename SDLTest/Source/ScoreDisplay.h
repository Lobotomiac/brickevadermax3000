#pragma once
#include "SDL.h"
#include "SDL_ttf.h"
#include "ScoreSystem.h"
#include <string>

class ScoreDisplay
{
public:
	ScoreDisplay();
	~ScoreDisplay();
	void Render(SDL_Renderer *Renderer);
	void Update();

private:
	TTF_Font* Font = nullptr;
	SDL_Texture *Texture = nullptr;
	SDL_Surface *ScoreSurface = nullptr;
	ScoreSystem* ScoreInstance = nullptr;
	
	SDL_Color FontColor;
	SDL_Rect DestinationRect;
	bool bShouldUpdateTexture;
	const char* FontFilePath = "Assets/stocky.ttf";
	int FontSize;
	int ScoreValue;
	std::string ScoreText;

};

