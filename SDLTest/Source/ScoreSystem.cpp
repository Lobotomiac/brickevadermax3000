#include "ScoreSystem.h"

ScoreSystem* ScoreSystem::Instance = nullptr;
Uint32 ScoreSystem::InstanceCounter = 0;

ScoreSystem::ScoreSystem()
{
	Score = 0;
	ScoreBaseValue = 0;
	bShouldUpdateScore = false;
}

ScoreSystem* ScoreSystem::GetInstance()
{
	InstanceCounter++;
	if (!Instance)
	{
		Instance = new ScoreSystem();
	}
	SDL_Log("ScoreSystem instances: %d", InstanceCounter);
	return Instance;
}

void ScoreSystem::ReleaseInstance()
{
	InstanceCounter--;
	if (Instance && InstanceCounter == 0)
	{
		delete Instance;
		Instance = nullptr;
	}
}

void ScoreSystem::SetScoreBaseValue(int Start)
{
	ScoreBaseValue = Start;
	bShouldUpdateScore = true;
}

void ScoreSystem::UpdateScore(int ReachedValue)
{
	if (bShouldUpdateScore && (ReachedValue < ScoreBaseValue))
	{
		bShouldUpdateScore = false;
		Score += ScoreBaseValue - ReachedValue;
	}
}