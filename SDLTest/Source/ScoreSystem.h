#pragma once
#include <SDL.h>

class ScoreSystem
{
public:
	static ScoreSystem* GetInstance();

	void ReleaseInstance();

	void SetScoreBaseValue(int Start);

	void UpdateScore(int ReachedValue);

	void ResetScore() { Score = 0; };

	int GetScore() const { return Score; };

private:
	ScoreSystem();
	~ScoreSystem() { ReleaseInstance(); };

private:
	int Score;
	int ScoreBaseValue;
	bool bShouldUpdateScore;

	static ScoreSystem* Instance;
	static Uint32 InstanceCounter;


};
