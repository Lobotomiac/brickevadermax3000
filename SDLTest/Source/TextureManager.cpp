#include "TextureManager.h"


SDL_Texture* TextureManager::LoadTexture(const char* FileName, SDL_Renderer* Renderer)
{
	SDL_Surface *TempSurface = IMG_Load(FileName);
	if (!TempSurface)
	{
		std::cout << "IMG_Load failed!" << SDL_GetError() << std::endl;
	}

	SDL_SetColorKey(TempSurface, SDL_TRUE, SDL_MapRGB(TempSurface->format, 255, 255, 255));

	SDL_Texture *Texture = SDL_CreateTextureFromSurface(Renderer, TempSurface);
	if (!Texture)
	{
		std::cout << "Failed to create Texture! SDL_Error: " << SDL_GetError() << std::endl;
	}
	SDL_FreeSurface(TempSurface);

	return Texture;
}