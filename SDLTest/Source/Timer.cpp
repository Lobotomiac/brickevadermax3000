#include "Timer.h"


Timer::Timer()
{
	StartTime = SDL_GetTicks();
	FrameStart = 0;
	FrameEnd = 0;
	UnpausedTime = 0;
	DeltaTime = 0;
}

void Timer::ResetTimer()
{
	FrameStart = 0;
	FrameEnd = 0;
	UnpausedTime = 0;
	DeltaTime = 0;
}

void Timer::UpdateTime(bool IsGamePaused)
{
	FrameEnd = SDL_GetTicks() - StartTime;

	DeltaTime = FrameEnd - FrameStart;
	if (DeltaTime < FrameDurationMS)
	{
		SDL_Delay(FrameDurationMS - DeltaTime);
	}

	FrameStart = SDL_GetTicks() - StartTime;
	
	if (!IsGamePaused)
	{
		UnpausedTime += FrameStart - FrameEnd + DeltaTime;
	}
}