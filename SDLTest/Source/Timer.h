#pragma once

#include "SDL.h"


class Timer
{
public:
	Timer();
	void ResetTimer();
	void UpdateTime(bool IsGamePaused);
	int GetUnpausedTime() const { return UnpausedTime; };

private:

	Uint32 FrameStart;
	Uint32 FrameEnd;
	int UnpausedTime;
	int DeltaTime;
	Uint32 StartTime;

	const int FPS = 60;
	const int FrameDurationMS = 1000 / FPS;


};