#include "Vector2D.h"

Vector2D::Vector2D()
{
	X = 0;
	Y = 0;
}

Vector2D::Vector2D(int argX, int argY)
{
	X = argX;
	Y = argY;
}


Vector2D Vector2D::operator+(const Vector2D & BVector)
{
	Vector2D TempVector;
	TempVector.X = this->X + BVector.X;
	TempVector.Y = this->Y + BVector.Y;
	return	TempVector;
}


Vector2D Vector2D::operator-(const Vector2D &BVector)
{
	Vector2D TempVector;
	TempVector.X = this->X - BVector.X;
	TempVector.Y = this->Y - BVector.Y;
	return TempVector;
}

Vector2D Vector2D::operator*(const Vector2D &BVector)
{
	Vector2D TempVector;
	TempVector.X = this->X * BVector.X;
	TempVector.Y = this->Y * BVector.Y;
	return TempVector;
}

Vector2D Vector2D::operator /(const Vector2D &BVector)
{
	Vector2D TempVector;
	if (this->X == 0 || BVector.X == 0)
	{
		TempVector.X = 0;
	}
	else
	{
		TempVector.X = this->X / BVector.X;
	}

	if (this->Y == 0 || BVector.Y == 0)
	{
		TempVector.Y = 0;
	}
	else
	{
		TempVector.Y = this->Y / BVector.Y;
	}
	return TempVector;
}

Vector2D & Vector2D::operator+=(const Vector2D &BVector)
{
	this->X += BVector.X;
	this->Y += BVector.Y;
	return *this;
}

Vector2D & Vector2D::operator-=(const Vector2D &BVector)
{
	this->X -= BVector.X;
	this->Y -= BVector.Y;
	return *this;
}
 
Vector2D & Vector2D::operator/=(const Vector2D &BVector)
{
	if (BVector.Y == 0)
	{
		this->Y = 0;
	}
	else
	{
		this->Y /= BVector.Y;
	}
	if (BVector.X == 0)
	{
		this->X = 0;
	}
	else
	{
		this->X /= BVector.X;
	}
	return *this;
}

Vector2D & Vector2D::operator *= (const Vector2D &BVector)
{
	this->X *= BVector.X;
	this->Y *= BVector.Y;
	return *this;
}

Vector2D& Vector2D::operator * (const int &i)
{
	this->X *= i;
	this->Y *= i;
	return *this;
}

Vector2D& Vector2D::operator / (const int &i)
{
	if (i)
	{
		this->X /= i;
		this->Y /= i;
	}
	else
	{
		this->X = 0;
		this->Y = 0;
	}
	return *this;
}

void Vector2D::SetZero()
{
	X = 0;
	Y = 0;
}