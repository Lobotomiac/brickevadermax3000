#pragma once

/*************************************************************************************************
 *************************************************************************************************
  We'll be using this class for Creating Vector2D class and to do it's math for easy Vector2D math
 *************************************************************************************************
 *************************************************************************************************/

class Vector2D
{
public:
	Vector2D();
	Vector2D(int argX, int argY);

	void SetZero();

	int X;
	int Y;

	Vector2D operator+(const Vector2D &BVector);
	Vector2D operator-(const Vector2D &BVector);
	Vector2D operator/(const Vector2D &BVector);
	Vector2D operator*(const Vector2D &BVector);
	
	Vector2D& operator+=(const Vector2D &BVector);
	Vector2D& operator-=(const Vector2D &BVector);
	Vector2D& operator*=(const Vector2D &BVector);
	Vector2D& operator/=(const Vector2D &BVector);
	
	Vector2D& operator*(const int &i);
	Vector2D& operator/(const int &i);

};